The Klocwork plugin for SonarQube allows for the import of issues from Klocwork 
server projects into SonarQube. These issues are raised as SonarQube issue and 
thus contribute to the metrics and quality gates of SonarQube. The plugin does 
not install the Klocwork tools or perform a Klocwork analysis; this will still 
need to be performed separately. The plugin uses the Klocwork API to retrieve 
the Klocwork server analysis results, then maps them to the files analyzed by 
SonarQube.

Also available is the Klocwork tools for SonarQube, which allows for various 
post analysis actions to further integrate Klocwork with SonarQube. The tool 
makes use of both the Klocwork API and SonarQube API to retrieve and update 
data for the issues and projects.
